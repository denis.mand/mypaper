interface FirebaseConfig {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  projectId: string;
  storageBucket: string;
  messagingSenderId: string;
  appId: string;
}
export interface EnvironmentInterface {
  production: boolean;
  firebaseConfig: FirebaseConfig;
}
