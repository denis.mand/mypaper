import { EnvironmentInterface } from './environment.interface';

export const environment: EnvironmentInterface = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyD5tPDk4Ze8lI0bknO9vfdVMNhXhuLx_KY',
    authDomain: 'mypaper-6db4e.firebaseapp.com',
    databaseURL: 'https://mypaper-6db4e.firebaseio.com',
    projectId: 'mypaper-6db4e',
    storageBucket: 'mypaper-6db4e.appspot.com',
    messagingSenderId: '932907577572',
    appId: '1:932907577572:web:3d72d202a7c84386167cf1',
  },
};
