import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavComponent } from './components/nav/nav.component';
import { FooterSocialGroupComponent } from '../core/footer/components/footer-social-group/footer-social-group.component';
import { IconsModule } from './components/svg/icons.module';
import { FooterContactUsComponent } from '../core/footer/components/footer-contact-us/footer-contact-us.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputComponent } from './components/controls/input/input.component';
import { HttpClientModule } from '@angular/common/http';
import { NavLinkComponent } from './components/nav/nav-link/nav-link.component';
import { TextareaComponent } from './components/controls/textarea/textarea.component';
import { QuizLinkComponent } from './components/quiz/quiz-link.component';
import { CardComponent } from './components/card/card.component';
import { CardGroupComponent } from './components/card-group/card-group.component';
import { RadioComponent } from './components/controls/radio/radio.component';
import { PortalModule } from '@angular/cdk/portal';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ModalComponent } from '../core/modal/modal.component';
@NgModule({
  declarations: [
    NavComponent,
    FooterSocialGroupComponent,
    FooterContactUsComponent,
    InputComponent,
    NavLinkComponent,
    TextareaComponent,
    RadioComponent,
    QuizLinkComponent,
    CardComponent,
    ModalComponent,
    CardGroupComponent,
  ],
  imports: [
    PortalModule,
    CommonModule,
    RouterModule,
    IconsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
  ],
  exports: [
    NavComponent,
    FooterSocialGroupComponent,
    FooterContactUsComponent,
    CommonModule,
    RouterModule,
    IconsModule,
    PortalModule,
    MatDialogModule,
    ReactiveFormsModule,
    InputComponent,
    HttpClientModule,
    NavLinkComponent,
    TextareaComponent,
    RadioComponent,
    QuizLinkComponent,
    CardComponent,
    ModalComponent,
    CardGroupComponent,
  ],
})
export class ShareModule {}
