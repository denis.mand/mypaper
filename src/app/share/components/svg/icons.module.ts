import { NgModule } from '@angular/core';
import { SvgTwitterComponent } from './social/svg-twitter.component';
import { SvgBrandLogoComponent } from './brand-logo/svg-brand-logo.component';
import { SvgFacebookComponent } from './social/svg-facebook.component';
import { SvgInstagramComponent } from './social/svg-instagram.component';
import { SvgLinkedInComponent } from './social/svg-linked-in.component';
import { SvgEmailComponent } from './contact/svg-email.component';
import { SvgMapComponent } from './contact/svg-map.component';
import { SvgPhoneComponent } from './contact/svg-phone.component';
import { SvgNavQuizComponent } from './nav/svg-nav-quiz.component';
import { SvgNavNewsComponent } from './nav/svg-nav-news.component';
import { CommonModule } from '@angular/common';
import { SvgNavDashboardComponent } from './nav/svg-nav-dashboard.component';
import { SvgCardQuizComponent } from './card/quiz/svg-card-quiz.component';

@NgModule({
  declarations: [
    SvgTwitterComponent,
    SvgBrandLogoComponent,
    SvgFacebookComponent,
    SvgInstagramComponent,
    SvgLinkedInComponent,
    SvgEmailComponent,
    SvgMapComponent,
    SvgPhoneComponent,
    SvgNavQuizComponent,
    SvgNavNewsComponent,
    SvgNavDashboardComponent,
    SvgCardQuizComponent,
  ],
  imports: [CommonModule],
  exports: [
    SvgTwitterComponent,
    SvgBrandLogoComponent,
    SvgFacebookComponent,
    SvgInstagramComponent,
    SvgLinkedInComponent,
    SvgEmailComponent,
    SvgMapComponent,
    SvgPhoneComponent,
    SvgNavQuizComponent,
    SvgNavNewsComponent,
    SvgNavDashboardComponent,
    SvgCardQuizComponent,
  ],
})
export class IconsModule {}
