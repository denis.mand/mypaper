import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-card-group',
  template: `<ng-content></ng-content>`,
  styleUrls: ['card-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardGroupComponent {}
