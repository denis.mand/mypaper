import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: 'card.component.html',
  styleUrls: ['card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
  @Input() id: string;
  @Input() authorName: string;
  @Input() authorPath: string;
  @Input() quizPath: string;
  @Input() name: string;
  @Input() date: string;

  get formattedName() {
    if (this.name.length < 120) return this.name;
    return `${this.name.slice(0, 120)}...`;
  }
}
