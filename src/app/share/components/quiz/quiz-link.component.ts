import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { QuizInterface } from '../../models/quiz/quiz.interface';
import { CREATE_QUIZ_PATH } from '../../../data/router-links/router-links';

@Component({
  selector: 'app-quiz-link',
  templateUrl: 'quiz-link.component.html',
  styleUrls: ['quiz-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuizLinkComponent {
  @Input() quiz: QuizInterface;
  @Input() path: string = CREATE_QUIZ_PATH;
}
