import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['../input/input.component.scss', './textarea.component.scss'],
})
export class TextareaComponent {
  @Input() group: FormGroup;
  @Input() label: string;
  @Input() name: string;
  @Input() placeholder: string;
  @Input() controlName: string;
  @Input() modification: string;
  @Input() cols: number = 40;
  @Input() rows: number = 3;
  @Input() maxLength: number;
  @Input() minLength: number;
}
