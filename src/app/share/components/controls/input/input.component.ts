import { Component, Input } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

export enum ValidatorsErrors {
  MISS_MATCH_PASSWORD = 'missMatchPassword',
}

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent {
  validatorsErrors = ValidatorsErrors;

  @Input() label: string;
  @Input() type: string = 'text';
  @Input() control: AbstractControl | FormControl;
}
