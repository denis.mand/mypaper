import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

export enum RadioThemes {
  LINE = 'line',
  LINE_DARK = 'line_dark',
  CIRCLE = 'circle',
}

@Component({
  selector: 'app-radio',
  template: `
    <label class="radio-control">
      <input
        class="radio-item"
        type="radio"
        [value]="value"
        [formControl]="control"
        [name]="name"
        [attr.disabled]="isDisabled || null"
      />
      <div class="radio-label">
        <ng-content></ng-content>
      </div>
    </label>
  `,
  styleUrls: ['radio.component.scss'],
})
export class RadioComponent {
  @Input() control: FormControl | AbstractControl;
  @Input() value: string | boolean;
  @Input() name: string;
  @Input() isDisabled: boolean = false;
}
