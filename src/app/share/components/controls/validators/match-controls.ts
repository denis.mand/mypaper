import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export function MustMatch(
  field: string,
  matchingField: string,
  errorName: string
): ValidatorFn {
  return (formGroup: FormGroup): ValidationErrors | null => {
    const control = formGroup.controls[field];
    const matchingControl = formGroup.controls[matchingField];

    if (matchingControl.errors && !matchingControl.errors[errorName]) {
      return null;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ [errorName]: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}
