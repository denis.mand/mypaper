import { FormArray, ValidationErrors, ValidatorFn } from '@angular/forms';

export function CheckRadio(
  controlName: string,
  errorName: string
): ValidatorFn {
  return (formArray: FormArray): ValidationErrors | null => {
    if (
      !formArray.value.length ||
      (formArray.errors && !formArray.errors[errorName])
    )
      return null;

    const radioChecked = formArray.value.some(
      (control) => control[controlName] === true
    );

    if (!radioChecked) {
      formArray.setErrors({ [errorName]: true });
    } else {
      formArray.setErrors(null);
    }
  };
}
