import { QuestionInterface } from './question.interface';

export interface QuizInterface {
  id?: string;
  name: string;
  date: string;
  questions: QuestionInterface[];
  userId: string;
  authorName: string;
}
