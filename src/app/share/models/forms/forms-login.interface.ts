export interface FormsLoginInterface {
  email: string;
  password: string;
  returnSecureToken: boolean;
}
