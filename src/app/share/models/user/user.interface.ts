export interface UserInterface {
  localId: string;
  email: string;
  emailVerified: boolean;
  displayName: string;
  providerUserInfo: any;
  photoUrl: string;
  passwordHash: string;
  passwordUpdatedAt: any;
  validSince: string;
  disabled: boolean;
  lastLoginAt: string;
  createdAt: string;
  customAuth: boolean;
}
