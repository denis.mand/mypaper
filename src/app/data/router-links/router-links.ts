export const HOME_PATH = '/';
export const ABOUT_US_PATH = '/about';
export const QUIZZES_PATH = '/quizzes';
export const QUIZ_PATH = (id) => `/quiz/${id}`;
export const AUTHOR_PATH = (id) => `/author/${id}`;
export const LOGIN_PATH = '/login';
export const REGISTRATION_PATH = '/registration';
export const ADMIN_PATH = '/admin';

// admin children
export const CREATE_QUIZ_PATH = 'create-quiz';
export const CREATE_NEWS_PATH = 'create-news';
