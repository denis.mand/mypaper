export const FORM_SENDING = 'Form is sending';
export const FORM_INVALID = 'Form is invalid';
export const FIRST_ITEM = 'This is first item';
export const LAST_ITEM = 'This is last item';
