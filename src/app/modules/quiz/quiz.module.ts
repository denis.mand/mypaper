import { NgModule } from '@angular/core';
import { QuizRoutingModule } from './quiz-routing.module';
import { QuizComponent } from './pages/quiz/quiz.component';
import { QuizAnswerComponent } from './components/quiz-answer/quiz-answer.component';
import { ShareModule } from '../../share/share.module';
import { QuizItemComponent } from './components/quiz-item/quiz-item.component';
import { QuizResultComponent } from './components/quiz-result/quiz-result.component';

@NgModule({
  imports: [ShareModule, QuizRoutingModule],
  declarations: [
    QuizComponent,
    QuizAnswerComponent,
    QuizItemComponent,
    QuizResultComponent,
  ],
})
export class QuizModule {}
