import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-quiz-answer',
  template: `{{ answer }}`,
})
export class QuizAnswerComponent {
  @Input() answer: string;
}
