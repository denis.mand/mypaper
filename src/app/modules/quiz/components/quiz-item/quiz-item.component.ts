import { Component, Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { QuestionControl } from '../../../../core/services/quiz/quiz-form-builder.service';
import { RadioThemes } from '../../../../share/components/controls/radio/radio.component';

@Component({
  selector: 'app-quiz-item',
  templateUrl: 'quiz-item.component.html',
  styleUrls: ['quiz-item.component.scss'],
})
export class QuizItemComponent {
  @Input() question: FormGroup | AbstractControl;
  @Input() index: number;
  @Input() isCompleted: boolean = false;
  questionControl = QuestionControl;
  radioTheme: string = `${RadioThemes.LINE} ${RadioThemes.LINE_DARK}`;

  getAnswerStatus(answerId: string) {
    if (this.isCompleted && answerId === this.question.value.chosenAnswer) {
      return answerId === this.question.value.rightAnswer
        ? 'is-right'
        : 'is-wrong';
    }
  }

  checkQuestionStatus() {
    if (this.isCompleted) {
      return this.question.value.rightAnswer ===
        this.question.value.chosenAnswer
        ? 'is-right'
        : 'is-wrong';
    }
  }

  getVariants(formArray): FormGroup[] {
    return formArray.controls as FormGroup[];
  }
}
