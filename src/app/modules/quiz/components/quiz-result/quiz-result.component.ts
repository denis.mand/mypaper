import { Component, Input } from '@angular/core';

export interface QuizResultInterface {
  rightQuestions: number;
  questions: number;
}

@Component({
  templateUrl: 'quiz-result.component.html',
})
export class QuizResultComponent {
  @Input() props: QuizResultInterface;
}
