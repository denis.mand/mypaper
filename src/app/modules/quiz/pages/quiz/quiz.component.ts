import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuizService } from '../../../../core/services/quiz/quiz.service';
import { Subject } from 'rxjs';
import { QuizInterface } from '../../../../share/models/quiz/quiz.interface';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import {
  AnswerControl,
  QuestionControl,
} from '../../../../core/services/quiz/quiz-form-builder.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../../../core/modal/modal.component';
import {
  QuizResultComponent,
  QuizResultInterface,
} from '../../components/quiz-result/quiz-result.component';

enum QuizForm {
  QUESTIONS = 'questions',
  ID = 'id',
}

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
})
export class QuizComponent implements OnInit, OnDestroy {
  destroyed$: Subject<boolean> = new Subject();
  quiz: QuizInterface;
  form: FormGroup;
  quizForm = QuizForm;
  quizResult: QuizResultInterface;
  isCompleted: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private quizApi: QuizService,
    private fb: FormBuilder,
    private modal: MatDialog
  ) {}

  ngOnInit() {
    this.getQuiz();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  private getQuiz() {
    this.route.params
      .pipe(
        switchMap((params) => {
          return this.quizApi.getQuiz(params.id);
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe((quiz) => {
        this.quiz = quiz;
        this.createForm(this.quiz);
      });
  }
  private createForm(quiz: QuizInterface) {
    this.form = this.fb.group({
      [QuizForm.ID]: this.fb.control(quiz.id),
      [QuizForm.QUESTIONS]: this.fb.array([]),
    });
    this.addQuestionsToForm(quiz.questions);
  }
  private addQuestionsToForm(questions) {
    for (const { rightAnswer, answers, question } of questions) {
      (this.form.get(QuizForm.QUESTIONS) as FormArray).push(
        this.fb.group({
          [QuestionControl.QUESTION]: this.fb.control(question),
          [QuestionControl.ANSWERS]: this.fb.array(
            this.addAnswersToForm(answers)
          ),
          [QuestionControl.CHOSEN_ANSWER]: this.fb.control(''),
          [QuestionControl.RIGHT_ANSWER]: this.fb.control(rightAnswer),
        })
      );
    }
  }
  private addAnswersToForm(answers) {
    const formGroups = [];
    for (const { answer, id } of answers) {
      formGroups.push(
        this.fb.group({
          [AnswerControl.ANSWER]: this.fb.control(answer),
          [AnswerControl.ID]: this.fb.control(id),
        })
      );
    }
    return formGroups;
  }
  private checkResult() {
    this.isCompleted = true;
    const { questions } = this.form.value;
    this.quizResult = {
      questions: questions.length,
      rightQuestions: questions.reduce((acc, question) => {
        if (question.chosenAnswer === question.rightAnswer) return acc + 1;
        return acc;
      }, 0),
    };
  }
  getVariants(formArray): FormGroup[] {
    return formArray.controls as FormGroup[];
  }

  complete() {
    this.checkResult();
    this.modal.open(ModalComponent, {
      data: { component: QuizResultComponent, props: this.quizResult },
    });
  }

  reset() {
    this.isCompleted = false;
    this.createForm(this.quiz);
  }
}
