import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizzesRoutingModule } from './quizzes-routing.module';
import { QuizzesComponent } from './pages/quizzes/quizzes.component';
import { ShareModule } from '../../share/share.module';

@NgModule({
  declarations: [QuizzesComponent],
  imports: [CommonModule, QuizzesRoutingModule, ShareModule],
  exports: [QuizzesComponent],
})
export class QuizzesModule {}
