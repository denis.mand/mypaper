import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { QuizService } from '../../../../core/services/quiz/quiz.service';
import { Observable } from 'rxjs';
import { QuizInterface } from '../../../../share/models/quiz/quiz.interface';
import {
  AUTHOR_PATH,
  QUIZ_PATH,
} from '../../../../data/router-links/router-links';

@Component({
  selector: 'app-quizzes',
  templateUrl: './quizzes.component.html',
  styleUrls: ['./quizzes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuizzesComponent implements OnInit {
  quizzes$: Observable<QuizInterface[]>;
  getQuizPath = QUIZ_PATH;
  getAuthorPath = AUTHOR_PATH;

  constructor(private quiz: QuizService) {}

  ngOnInit(): void {
    this.quizzes$ = this.quiz.getAllQuizzes();
  }
}
