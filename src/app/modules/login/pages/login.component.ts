import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth/auth.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ADMIN_PATH } from '../../../data/router-links/router-links';
import { SESSION_FINISHED } from '../../../data/navigation/navigation-query-params';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

enum LoginControls {
  EMAIL = 'email',
  PASSWORD = 'password',
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit, OnDestroy {
  form: FormGroup;
  isSubmitted: boolean = false;
  errorMessage: string;
  destroyed$: Subject<boolean> = new Subject();
  loginControls = LoginControls;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.createForm();
    this.checkParams();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  private createForm() {
    this.form = this.fb.group({
      [LoginControls.EMAIL]: ['', [Validators.email, Validators.required]],
      [LoginControls.PASSWORD]: [
        null,
        [Validators.required, Validators.minLength(6)],
      ],
    });
  }
  private checkParams() {
    this.route.queryParams
      .pipe(takeUntil(this.destroyed$))
      .subscribe((params: Params) => {
        if (params.error && params.error === SESSION_FINISHED) {
          this.errorMessage = 'The session has expired. Please enter data';
        }
      });
  }
  submit() {
    this.isSubmitted = true;

    const data = {
      email: this.form.value.email,
      password: this.form.value.password,
      returnSecureToken: true,
    };

    this.auth
      .login(data)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (res) => {
          this.form.reset();
          this.router.navigate([ADMIN_PATH]);
        },
        () => {
          this.isSubmitted = false;
        }
      );
  }
}
