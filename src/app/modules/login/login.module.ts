import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './pages/login.component';
import { ShareModule } from '../../share/share.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [ShareModule, LoginRoutingModule],
})
export class LoginModule {}
