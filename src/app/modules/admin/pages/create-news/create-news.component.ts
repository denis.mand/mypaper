import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNewsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
