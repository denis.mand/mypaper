import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FORM_INVALID,
  FORM_SENDING,
} from '../../../../data/tooltip/tooltip-text';
import { QuizService } from '../../../../core/services/quiz/quiz.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import {
  FormControls,
  QuestionControl,
  QuizFormBuilderService,
} from '../../../../core/services/quiz/quiz-form-builder.service';
import { ADMIN_PATH } from '../../../../data/router-links/router-links';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../../core/services/user/user.service';
import { RadioThemes } from '../../../../share/components/controls/radio/radio.component';

@Component({
  selector: 'app-create-quiz',
  templateUrl: './create-quiz.component.html',
  styleUrls: ['./create-quiz.component.scss'],
})
export class CreateQuizComponent implements OnInit, OnDestroy {
  quizId: string;
  form: FormGroup;
  indexOfActiveQuestion: number;
  isSubmitted: boolean = false;
  error: string;
  destroyed$: Subject<boolean> = new Subject();
  formControls = FormControls;
  questionControls = QuestionControl;
  radioTheme: string = RadioThemes.CIRCLE;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private quiz: QuizService,
    private userApi: UserService,
    private quizFormBuilder: QuizFormBuilderService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form = this.quizFormBuilder.createForm();
    this.checkRouteParams();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  get hasQuestions(): boolean {
    return !!this.form.get(FormControls.QUESTIONS).value.length;
  }

  get disabledText(): string {
    return this.isSubmitted ? FORM_SENDING : FORM_INVALID;
  }

  checkRouteParams() {
    try {
      this.route.params
        .pipe(
          switchMap((params) => {
            if (!params.id) return of(null);
            return this.quiz.getQuiz(params.id);
          }),
          takeUntil(this.destroyed$)
        )
        .subscribe((quiz) => {
          if (quiz) {
            this.quizId = quiz.id;
            this.quizFormBuilder.setQuizValueToForm(this.form, quiz);
            this.indexOfActiveQuestion = 0;
          }
        });
    } catch (e) {}
  }

  addQuestion() {
    this.quizFormBuilder.addQuestion(this.form);
    this.indexOfActiveQuestion = this.form.value.questions.length - 1;
  }

  getVariants(formArray): FormGroup[] {
    return formArray.controls as FormGroup[];
  }

  removeQuestion() {
    this.quizFormBuilder.removeQuestion(this.form, this.indexOfActiveQuestion);

    if (this.indexOfActiveQuestion > 0) {
      this.indexOfActiveQuestion--;
    } else {
      this.indexOfActiveQuestion = 0;
    }
  }
  toggleActiveQuestion(status) {
    status ? this.indexOfActiveQuestion++ : this.indexOfActiveQuestion--;
  }
  saveQuiz() {
    this.isSubmitted = true;
    const form = { ...this.form.value };
    form.userId = this.userApi.user.localId;
    form.authorName = this.userApi.user.displayName || 'Anonymous';

    const saveQuiz$ = this.quizId
      ? this.quiz.edit(form, this.quizId)
      : this.quiz.create(form);

    saveQuiz$.pipe(takeUntil(this.destroyed$)).subscribe(
      () => {
        this.form.reset();
        this.toastr.success('Quiz is saved');
        this.router.navigate([ADMIN_PATH]);
      },
      () => {
        this.toastr.error('We have a problem');
      },
      () => {
        this.isSubmitted = false;
      }
    );
  }
  removeQuiz() {
    this.isSubmitted = true;
    this.quiz.remove(this.quizId).subscribe(
      () => {
        this.toastr.success('Quiz is removed');
        this.router.navigate([ADMIN_PATH]);
      },
      () => {
        this.toastr.error('We have a problem');
      },
      () => {
        this.isSubmitted = false;
      }
    );
  }
}
