import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { QuizService } from '../../../../core/services/quiz/quiz.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { QuizInterface } from '../../../../share/models/quiz/quiz.interface';
import { UserService } from '../../../../core/services/user/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
  quizzes$: Observable<QuizInterface[]>;

  constructor(private quiz: QuizService, private userApi: UserService) {}

  ngOnInit(): void {
    this.quizzes$ = this.quiz
      .getUserQuizzes(this.userApi.user.localId)
      .pipe(map((quizzes) => quizzes.reverse()));
  }
}
