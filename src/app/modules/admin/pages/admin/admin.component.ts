import { Component } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  isCollapsedMenu: boolean = true;

  toggleMenu() {
    this.isCollapsedMenu = !this.isCollapsedMenu;
  }
}
