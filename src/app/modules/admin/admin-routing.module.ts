import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './pages/admin/admin.component';
import { CreateQuizComponent } from './pages/create-quiz/create-quiz.component';
import { CreateNewsComponent } from './pages/create-news/create-news.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'create-quiz/:id', component: CreateQuizComponent },
      { path: 'create-quiz', component: CreateQuizComponent },
      { path: 'create-news', component: CreateNewsComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
