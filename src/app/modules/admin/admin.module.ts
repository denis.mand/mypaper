import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './pages/admin/admin.component';
import { ShareModule } from '../../share/share.module';
import { CreateQuizComponent } from './pages/create-quiz/create-quiz.component';
import { CreateNewsComponent } from './pages/create-news/create-news.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { SideMenuLinkComponent } from './components/side-menu-link/side-menu-link.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DashboardColumnComponent } from './components/dashboard-column/dashboard-column.component';
import { CreateQuizNavComponent } from './components/create-quiz-nav/create-quiz-nav.component';

@NgModule({
  declarations: [
    AdminComponent,
    CreateQuizComponent,
    CreateNewsComponent,
    SideMenuComponent,
    SideMenuLinkComponent,
    DashboardComponent,
    DashboardColumnComponent,
    CreateQuizNavComponent,
  ],
  imports: [ShareModule, AdminRoutingModule],
})
export class AdminModule {}
