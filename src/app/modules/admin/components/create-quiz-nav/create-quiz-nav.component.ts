import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  FIRST_ITEM,
  FORM_INVALID,
  LAST_ITEM,
} from '../../../../data/tooltip/tooltip-text';

@Component({
  selector: 'app-create-quiz-nav',
  templateUrl: './create-quiz-nav.component.html',
  styleUrls: ['./create-quiz-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateQuizNavComponent {
  @Input() indexOfActiveQuestion: number;
  @Input() length: number;
  @Input() isFormInvalid: boolean;
  @Output() removeQuestion: EventEmitter<any> = new EventEmitter();
  @Output() addQuestion: EventEmitter<any> = new EventEmitter();
  @Output() toggleActiveQuestion: EventEmitter<any> = new EventEmitter();

  tooltipText: string = FORM_INVALID;

  get isPrevButtonDisabled() {
    return this.isFormInvalid || this.indexOfActiveQuestion < 1;
  }

  get prevBtnTooltipText() {
    return this.isPrevButtonDisabled
      ? this.indexOfActiveQuestion < 1
        ? FIRST_ITEM
        : FORM_INVALID
      : '';
  }
  get nextBtnTooltipText() {
    return this.isNextButtonDisabled
      ? this.indexOfActiveQuestion >= this.length - 1
        ? LAST_ITEM
        : FORM_INVALID
      : '';
  }

  get isNextButtonDisabled() {
    return this.indexOfActiveQuestion >= this.length - 1 || this.isFormInvalid;
  }
}
