import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  ADMIN_PATH,
  CREATE_NEWS_PATH,
  CREATE_QUIZ_PATH,
} from '../../../../data/router-links/router-links';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SideMenuComponent {
  public CREATE_NEWS_PATH: string = CREATE_NEWS_PATH;
  public CREATE_QUIZ_PATH: string = CREATE_QUIZ_PATH;
  public ADMIN_PATH: string = ADMIN_PATH;

  @Input() isCollapsed: boolean;
  @Output() toggleMenu: EventEmitter<any> = new EventEmitter();
}
