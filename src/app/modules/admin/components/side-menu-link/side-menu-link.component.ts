import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-side-menu-link',
  template: `
    <a
      [routerLink]="[path]"
      routerLinkActive="active"
      [routerLinkActiveOptions]="{ exact: true }"
      class="side-menu__link"
    >
      <ng-content></ng-content>
      <span class="side-menu__link-name" [class.collapsed]="isCollapsed">
        {{ name }}
      </span>
    </a>
  `,
  styleUrls: ['../side-menu/side-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SideMenuLinkComponent {
  @Input() name: string;
  @Input() path: string = '/admin';
  @Input() isCollapsed: boolean;
}
