import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../../../share/components/controls/validators/match-controls';
import { AuthService } from '../../../../core/services/auth/auth.service';
import { Router } from '@angular/router';
import { ADMIN_PATH } from '../../../../data/router-links/router-links';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ValidatorsErrors } from '../../../../share/components/controls/input/input.component';

enum RegistrationControls {
  EMAIL = 'email',
  PASSWORD = 'password',
  PASSWORD_CONFIRM = 'passwordConfirm',
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit, OnDestroy {
  form: FormGroup;
  isSubmitted: boolean = false;
  destroyed$: Subject<boolean> = new Subject();
  registrationControls = RegistrationControls;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group(
      {
        [RegistrationControls.EMAIL]: [
          '',
          [Validators.email, Validators.required],
        ],
        [RegistrationControls.PASSWORD]: [
          null,
          [Validators.required, Validators.minLength(6)],
        ],
        [RegistrationControls.PASSWORD_CONFIRM]: [
          null,
          [Validators.required, Validators.minLength(6)],
        ],
      },
      {
        validators: MustMatch(
          RegistrationControls.PASSWORD,
          RegistrationControls.PASSWORD_CONFIRM,
          ValidatorsErrors.MISS_MATCH_PASSWORD
        ),
      }
    );
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  submit() {
    this.isSubmitted = true;
    const data = {
      email: this.form.value.email,
      password: this.form.value.password,
      returnSecureToken: true,
    };
    this.auth
      .register(data)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => {
          this.form.reset();
          this.router.navigate([ADMIN_PATH]);
        },
        () => (this.isSubmitted = false)
      );
  }
}
