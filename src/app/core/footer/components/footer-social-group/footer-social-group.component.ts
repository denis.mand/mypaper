import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-footer-social-group',
  templateUrl: './footer-social-group.component.html',
  styleUrls: ['./footer-social-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterSocialGroupComponent {}
