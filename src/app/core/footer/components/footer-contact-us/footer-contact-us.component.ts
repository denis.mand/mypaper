import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-footer-contact-us',
  templateUrl: './footer-contact-us.component.html',
  styleUrls: ['./footer-contact-us.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterContactUsComponent {}
