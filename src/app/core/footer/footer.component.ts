import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  ABOUT_US_PATH,
  ADMIN_PATH,
  HOME_PATH,
  LOGIN_PATH,
  QUIZZES_PATH,
  REGISTRATION_PATH,
} from '../../data/router-links/router-links';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {
  public HOME_PATH: string = HOME_PATH;
  public ABOUT_US_PATH: string = ABOUT_US_PATH;
  public QUIZZES_PATH: string = QUIZZES_PATH;
  public LOGIN_PATH: string = LOGIN_PATH;
  public REGISTRATION_PATH: string = REGISTRATION_PATH;
  public ADMIN_PATH: string = ADMIN_PATH;
  public isHoveredLogo: boolean = false;

  constructor(public auth: AuthService) {}

  get isAuthenticated(): boolean {
    return this.auth.isAuthenticated();
  }

  toggleHoveredStatus(status: boolean) {
    this.isHoveredLogo = status;
  }
}
