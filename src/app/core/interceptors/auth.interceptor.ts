import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SESSION_FINISHED } from '../../data/navigation/navigation-query-params';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const idToken = localStorage.getItem('token');

    if (idToken) {
      const cloned = req.clone({
        setParams: {
          auth: idToken,
        },
      });

      return next.handle(cloned).pipe(
        catchError((err: HttpErrorResponse) => {
          this.auth.logout(SESSION_FINISHED);
          return throwError(err);
        })
      );
    } else {
      return next.handle(req);
    }
  }
}
