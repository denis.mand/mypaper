export function getLoginErrorMessage(message: string): string {
  const map = new Map([
    ['INVALID_EMAIL', 'Wrong email'],
    ['INVALID_PASSWORD', 'Wrong password'],
    ['EMAIL_NOT_FOUND', 'Email not found'],
  ]);

  return map.get(message) || '';
}

export function getRegisterErrorMessage(message: string): string {
  const map = new Map([
    ['EMAIL_EXISTS', 'The email address is already in use by another account'],
    ['OPERATION_NOT_ALLOWED', 'Password sign-in is disabled for this project'],
    [
      'TOO_MANY_ATTEMPTS_TRY_LATER',
      'We have blocked all requests from this device due to unusual activity. Try again later',
    ],
  ]);

  return map.get(message) || '';
}
