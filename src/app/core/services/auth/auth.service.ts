import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { FormsLoginInterface } from '../../../share/models/forms/forms-login.interface';
import { environment } from '../../../../environments/environment';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOGIN_PATH } from '../../../data/router-links/router-links';
import { LoginResponseInterface } from './auth.interface';
import {
  getLoginErrorMessage,
  getRegisterErrorMessage,
} from './helpers/auth-errors.helper';
import { SESSION_FINISHED } from '../../../data/navigation/navigation-query-params';

const LOGIN = 'login';
const REGISTRATION = 'registration';

@Injectable()
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {}

  get _token(): string {
    if (
      !(localStorage.getItem('token') && localStorage.getItem('token-expires'))
    )
      return null;

    const expDate = new Date(localStorage.getItem('token-expires'));

    if (new Date() > expDate) {
      this.logout(SESSION_FINISHED);
      return null;
    }
    return localStorage.getItem('token');
  }

  isAuthenticated(): boolean {
    return !!this._token;
  }

  login(user: FormsLoginInterface): Observable<LoginResponseInterface> {
    return this.http
      .post(
        `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.firebaseConfig.apiKey}`,
        user
      )
      .pipe(
        tap(this.setToken),
        catchError((error) => this.handleError(error, LOGIN))
      );
  }

  register(user: FormsLoginInterface): Observable<any> {
    return this.http
      .post(
        `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.firebaseConfig.apiKey}`,
        user
      )
      .pipe(
        tap(this.setToken),
        catchError((error) => this.handleError(error, REGISTRATION))
      );
  }

  logout(params?: string) {
    this.setToken(null);
    this.router.navigate([LOGIN_PATH], { queryParams: { error: params } });
  }

  private setToken(response: LoginResponseInterface | null) {
    if (!response) return this.clearToken();
    const expDate = new Date(new Date().getTime() + +response.expiresIn * 1000);
    localStorage.setItem('token', response.idToken);
    localStorage.setItem('refresh-token', response.refreshToken);
    localStorage.setItem('token-expires', expDate.toString());
  }

  private clearToken() {
    localStorage.removeItem('token');
    localStorage.removeItem('refresh-token');
    localStorage.removeItem('token-expires');
  }

  private handleError(
    error: HttpErrorResponse,
    type: string
  ): Observable<never> {
    const {
      error: {
        error: { message },
      },
    } = error;

    this.toastr.error(this.getErrorMessage(message, type));
    return throwError(error);
  }

  private getErrorMessage(message: string, type: string): string {
    return type === LOGIN
      ? getLoginErrorMessage(message)
      : getRegisterErrorMessage(message);
  }
}
