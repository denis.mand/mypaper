import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { UserInterface } from '../../../share/models/user/user.interface';
import { map } from 'rxjs/operators';

interface GetUserInterface {
  kind: string;
  users: UserInterface[];
}

@Injectable()
export class UserService {
  key: string = '?key=AIzaSyD5tPDk4Ze8lI0bknO9vfdVMNhXhuLx_KY';
  api: string = 'https://identitytoolkit.googleapis.com/v1/accounts:';

  private readonly _user = new BehaviorSubject<UserInterface>(null);

  readonly user$ = this._user.asObservable();

  get user(): UserInterface {
    return this._user.getValue();
  }

  saveUser(user: UserInterface) {
    if (!this.user) this._user.next(user);
  }

  clearUser() {
    this._user.next(null);
  }

  constructor(private http: HttpClient) {}

  getUser(idToken) {
    return this.http
      .post<GetUserInterface>(`${this.api}lookup${this.key}`, {
        idToken,
      })
      .pipe(
        map(({ users }) => {
          this.saveUser(users[0]);
        })
      );
  }
}
