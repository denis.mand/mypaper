import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Injectable } from '@angular/core';
import { QuizInterface } from '../../../share/models/quiz/quiz.interface';

export enum FormControls {
  NAME = 'name',
  QUESTIONS = 'questions',
  DATE = 'date',
}

export enum AnswerControl {
  ID = 'id',
  ANSWER = 'answer',
}

export enum QuestionControl {
  ID = 'id',
  QUESTION = 'question',
  RIGHT_ANSWER = 'rightAnswer',
  CHOSEN_ANSWER = 'chosenAnswer',
  ANSWERS = 'answers',
}

@Injectable()
export class QuizFormBuilderService {
  constructor(private fb: FormBuilder) {}

  createForm() {
    return this.fb.group({
      [FormControls.NAME]: new FormControl('', [Validators.required]),
      [FormControls.QUESTIONS]: new FormArray([]),
      [FormControls.DATE]: [new Date()],
    });
  }

  setQuizValueToForm(form: FormGroup, quiz: QuizInterface) {
    form.get(FormControls.NAME).setValue(quiz.name);
    form.get(FormControls.DATE).setValue(quiz.date);
    quiz.questions.forEach((question, index) => {
      this.addQuestion(form);
      const questionsControl = (form.get(FormControls.QUESTIONS) as FormArray)
        .controls[index];
      questionsControl.get(QuestionControl.ID).setValue(question.id);
      questionsControl
        .get(QuestionControl.QUESTION)
        .setValue(question.question);
      questionsControl
        .get(QuestionControl.RIGHT_ANSWER)
        .setValue(question.rightAnswer);
      (questionsControl.get(
        QuestionControl.ANSWERS
      ) as FormArray).controls.forEach((item, answerIndex) => {
        item.patchValue({
          id: question.answers[answerIndex].id,
          answer: question.answers[answerIndex].answer,
        });
      });
    });
  }

  addQuestion(form) {
    (form.get(FormControls.QUESTIONS) as FormArray).push(
      this.fb.group({
        [QuestionControl.ID]: form.get(FormControls.QUESTIONS).value.length + 1,
        [QuestionControl.QUESTION]: ['', [Validators.required]],
        [QuestionControl.ANSWERS]: this.fb.array([
          this.fb.group({
            [AnswerControl.ID]: 'a',
            [AnswerControl.ANSWER]: ['', [Validators.required]],
          }),
          this.fb.group({
            [AnswerControl.ID]: 'b',
            [AnswerControl.ANSWER]: ['', [Validators.required]],
          }),
          this.fb.group({
            [AnswerControl.ID]: 'c',
            [AnswerControl.ANSWER]: ['', [Validators.required]],
          }),
          this.fb.group({
            [AnswerControl.ID]: 'd',
            [AnswerControl.ANSWER]: ['', [Validators.required]],
          }),
        ]),
        [QuestionControl.RIGHT_ANSWER]: ['', [Validators.required]],
      })
    );
  }

  removeQuestion(form, index) {
    (form.get(FormControls.QUESTIONS) as FormArray).removeAt(index);
  }
}
