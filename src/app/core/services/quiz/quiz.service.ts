import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuizInterface } from '../../../share/models/quiz/quiz.interface';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface CreateApiInterface {
  name: string;
}

@Injectable()
export class QuizService {
  quizzes: QuizInterface[];

  constructor(private http: HttpClient) {}

  create(quiz: QuizInterface): Observable<CreateApiInterface> {
    return this.http.post<CreateApiInterface>(
      `${environment.firebaseConfig.databaseURL}/quizzes.json`,
      quiz
    );
  }

  edit(form: QuizInterface, id: string): Observable<QuizInterface> {
    return this.http
      .put<QuizInterface>(
        `${environment.firebaseConfig.databaseURL}/quizzes/${id}.json`,
        form
      )
      .pipe(
        map((response) => ({
          ...response,
          id,
        }))
      );
  }

  remove(id): Observable<null> {
    return this.http.delete<null>(
      `${environment.firebaseConfig.databaseURL}/quizzes/${id}.json`
    );
  }

  getQuiz(id): Observable<QuizInterface> {
    return this.http
      .get<QuizInterface>(
        `${environment.firebaseConfig.databaseURL}/quizzes/${id}.json`
      )
      .pipe(
        map((response) => ({
          ...response,
          id,
        }))
      );
  }

  getAllQuizzes(): Observable<QuizInterface[]> {
    return this.http
      .get(`${environment.firebaseConfig.databaseURL}/quizzes.json`)
      .pipe(
        map((response) => {
          return Object.keys(response).reduce((acc, item) => {
            acc.push({
              id: item,
              ...response[item],
            });
            return acc;
          }, []);
        })
      );
  }
  getUserQuizzes(id): Observable<QuizInterface[]> {
    return this.getAllQuizzes().pipe(
      map((item) => item.filter((quiz) => quiz.userId === id))
    );
  }
}
