import { Component, ComponentRef, Inject, OnInit } from '@angular/core';
import {
  CdkPortalOutletAttachedRef,
  ComponentPortal,
} from '@angular/cdk/portal';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface ModalState {
  open: boolean;
  position: ModalPosition;
}

export enum ModalPosition {
  CENTER = 'center',
  RIGHT = 'right',
}

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.scss'],
})
export class ModalComponent implements OnInit {
  portal: ComponentPortal<any>;

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.portal = new ComponentPortal(this.data.component);
  }

  onClickClose() {
    this.dialogRef.close();
  }

  foo(ref: CdkPortalOutletAttachedRef) {
    ref = ref as ComponentRef<any>;
    if (this.data.props) {
      ref.instance.props = this.data.props;
    }
  }
}
