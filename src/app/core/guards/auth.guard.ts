import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { LOGIN_PATH } from '../../data/router-links/router-links';

@Injectable()
export class AuthGuard {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate([LOGIN_PATH]);
      return false;
    }
    return true;
  }
}
