import { NgModule, Optional, Provider, SkipSelf } from '@angular/core';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ShareModule } from '../share/share.module';
import { AuthService } from './services/auth/auth.service';
import { QuizService } from './services/quiz/quiz.service';
import { UserService } from './services/user/user.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { AuthGuard } from './guards/auth.guard';
import { QuizFormBuilderService } from './services/quiz/quiz-form-builder.service';
import { UserResolver } from './services/user/user.resolver';

const AUTH_INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor,
};

@NgModule({
  declarations: [FooterComponent, HeaderComponent],
  imports: [ShareModule],
  providers: [
    AuthService,
    QuizService,
    QuizFormBuilderService,
    UserService,
    AUTH_INTERCEPTOR_PROVIDER,
    AuthGuard,
    UserResolver,
  ],
  exports: [HeaderComponent, FooterComponent],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only'
      );
    }
  }
}
